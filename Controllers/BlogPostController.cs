using Microsoft.AspNetCore.Mvc;
using hello_blog_api.Models;
using hello_blog_api.TestData;
using System.Net;
using System.Collections.Generic;

namespace hello_blog_api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class BlogPostController: Controller
    {
        ///<summary>
        ///Creates a blog using the payload information.
        ///Endpoint url: api/v1/BlogPost
        ///</summary>
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Created, Type = typeof(BlogPostModel))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public IActionResult CreateBlogPost([FromBody] BlogPostModel blogPost)
        {
            try
            {
                if (!BlogPostTestData.ValidateBlogPostModel(blogPost))
                {
                    return BadRequest("Model is not valid!");
                }
               
                if(!BlogPostTestData.AddBlogPost(blogPost))
                {   
                     return StatusCode((int)HttpStatusCode.InternalServerError);
                }
                
                return CreatedAtAction(nameof(CreateBlogPost), blogPost);
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        ///<summary>
        ///Gets a list of all blogs.
        ///Endpoint url: api/v1/BlogPost
        ///</summary>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public IActionResult GetAllBlogPosts()
        {
            try
            {
                List<BlogPostModel> blogPosts = BlogPostTestData.GetAllBlogPosts();
                return Ok(blogPosts);
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        ///<summary>
        ///Gets a blog based on the blogId.
        ///Endpoint url: api/v1/BlogPost/{blogId}
        ///</summary>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Route("{blogPostId}")]
        public IActionResult GetBlogPostById([FromRoute] int blogPostId)
        {
            try
            {
                BlogPostModel blogPost = BlogPostTestData.GetBlogPostById(blogPostId);
                if (blogPost == null)
                {
                    return StatusCode((int)HttpStatusCode.NotFound);
                }
                return Ok(blogPost);
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }
        [HttpGet]
        [Route("tag/{blogPostTag}")]
        public IActionResult GetBlogPostByTag([FromRoute] string blogPostTag)
        {
            try
            {
                List<BlogPostModel> blogPosts = BlogPostTestData.getBlogPostsByTag(blogPostTag);
                if(blogPosts == null)
                {
                    return StatusCode((int)HttpStatusCode.NotFound);
                } else 
                {
                    return Ok(blogPosts);
                }
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }
        [HttpDelete]
        [Route("{blogPostId}")]
        public IActionResult DeleteBlogPostById([FromRoute] int blogPostId)
        {
            try{
                if(BlogPostTestData.DeleteBlogPostById(blogPostId))
                {
                    return Ok();
                } else 
                {
                    return StatusCode((int)HttpStatusCode.NotFound);
                }
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        [HttpPut]
        [Route("{blogPostId}")]
        public IActionResult UpdateBlogPostById([FromRoute] int blogPostId, [FromBody] BlogPostModel blogPost)
        {
            try
            {
                if (!BlogPostTestData.ValidateBlogPostModel(blogPost))
                {
                    return BadRequest("Model is not valid!");
                }
                if(!BlogPostTestData.ValidateId(blogPostId))
                {
                    return StatusCode((int)HttpStatusCode.NotFound);
                }
                if(!BlogPostTestData.UpdateBlogPostById(blogPostId, blogPost))
                {
                    return StatusCode((int)HttpStatusCode.InternalServerError);
                }
                return Ok(blogPost);
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }
    }
}