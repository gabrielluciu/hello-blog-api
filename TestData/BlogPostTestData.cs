using hello_blog_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace hello_blog_api.TestData
{
    public static class BlogPostTestData
    {
         ///<summary>Increments the Id of each blog post when a new record is created.</summary>
        private static int _id = 0;

        private static List<BlogPostModel> _blogPosts = new List<BlogPostModel>();

        public static BlogPostModel GetBlogPostById(int id)
        {
            BlogPostModel blogPostModel =_blogPosts.FirstOrDefault(blogPost => blogPost.Id == id);
            return blogPostModel;
        }

        public static List<BlogPostModel> GetAllBlogPosts()
        {
            return _blogPosts;
        }

        public static bool AddBlogPost(BlogPostModel blogPost)
        {
            blogPost.Id = ++_id;
            _blogPosts.Add(blogPost);
            return true;
        }

        public static bool ValidateBlogPostModel(BlogPostModel blogPost)
        {
            bool isValid = true;
            if (string.IsNullOrWhiteSpace(blogPost.Title))
            {
                isValid=false;
            }
            if (string.IsNullOrWhiteSpace(blogPost.Content))
            {
                isValid=false;
            }
            if (string.IsNullOrWhiteSpace(blogPost.Label))
            {
                isValid=false;
            }
            return isValid;
        }

        public static List<BlogPostModel> getBlogPostsByTag(string blogPostTag)
        {
            List<BlogPostModel> blogPosts = _blogPosts.FindAll(post => post.Label == blogPostTag);
            return blogPosts;
            //throw new NotImplementedException();
        }
        public static bool DeleteBlogPostById(int id)
        {
            BlogPostModel blogPostToRemove = _blogPosts.Find(blogPost => blogPost.Id == id);
            return _blogPosts.Remove(blogPostToRemove);
        }

        internal static bool UpdateBlogPostById(int id, BlogPostModel blogPost)
        {
            BlogPostModel blogPostToUpdate = _blogPosts.Find(post => post.Id == id);

            if(_blogPosts == null) 
            {
                return false;
            }
            UpdateBlogPost(blogPostToUpdate, blogPost);
            return true;
        }

        private static void UpdateBlogPost(BlogPostModel blogPostToUpdate, BlogPostModel blogPost)
        {
            blogPostToUpdate.Label = blogPost.Label;
            blogPostToUpdate.Title = blogPost.Title;
            blogPostToUpdate.Content = blogPost.Content;
        }

        internal static bool ValidateId(int id)
        {
            if(id <= 0 || id > _id || _blogPosts.Find(post => post.Id == id) == null)
            {
                return false;
            }
            return true;
        }
    }
}